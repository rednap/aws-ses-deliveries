# AWS SES Delivery Tracker

Chalice/Lambda App that waits for SNS notification event from AWS SES, reads the JSON object and then puts data in a DynamoDB table

Before you install, ensure you have installed and have a basic working knowledge in:  
[python3](https://www.python.org/downloads/)  
[Chalice](https://chalice.readthedocs.io/en/latest/)  
[An AWS Account](https://aws.amazon.com/console/)  
Assumption: You are already using AWS SES to send emails

Clone this repo onto your computer:

```
 $ git clone https://rednap@bitbucket.org/rednap/aws-ses-deliveries.git aws-ses-deliveries
```

### Prerequisite: Set up SNS Topic

1. Log into the AWS Console, and head over to [SNS](https://console.aws.amazon.com/sns/)
2. Click on "Topics" then "Create Topic" 
3. Give your topic a name and then scroll down and click "Create Topic". Take note of the name you use for later.

### Prerequisite: Set up DynamoDB table

1. now head to the [DynamoDB Console](https://console.aws.amazon.com/dynamodb/)
2. Click "Create Table"
3. Give your table a name and take note of the name you give it for later.
4. for Primary Key type "id" with type "String"
5. click "Create"

### App Config

Change into the project directory:

```
 $ cd aws-ses-deliveries
```

Edit app.py with your editor of choice, and edit the following lines of code to reflect your name choices:

```
 14 @app.on_sns_message(topic='your-sns-topic')
...
 31     table = dynamo.Table('your-dynamodb-table')
```

Finally, change into the .chalice directory, and copy the config file from the default version:

```
 $ cd .chalice
 $ cp config.json.default config.json
```

Edit config.json and input your own AWS credentials:

```
 10         "ACCESS_ID": "XXXXXXX_YOUR_AWS_ID_XXXX",
 11         "ACCESS_KEY": "tasdffJHGJHDJhgic78yk_your_aws_secret_key"
```

### Deploy

Once the app is configured, simply change back to the project root, and run "chalice deploy"

```
 $ cd ..
 $ chalice deploy
```

Once installed you can verify the lambda function exists in your [AWS Console](https://console.aws.amazon.com/lambda/), and verify it's working by sending an email, and then either checking your dynamoDB table, or cloudwatch. Happy emailing!

## Built With

* [Python](https://www.python.org/) - Everyone's favorite!
* [AWS](https://aws.amazon.com/) - Cloud Computing Services
* [Chalice](https://chalice.readthedocs.io/en/latest/) - Flask-like framework to quickly create and deploy applications that use Amazon API Gateway and AWS Lambda.

## Authors

* **Andy Klier** - *Initial work* - [http://www.andyklier.com/](http://www.andyklier.com/)
